import * as nodemailer from "nodemailer";
import smtpTransport from "nodemailer-smtp-transport"
import * as Handlebars from "handlebars";
import * as fs from "fs"
import * as path from "path"


const createContractTemplateSource = fs.readFileSync(path.join(__dirname, "./mail-templates/contract.hbs"), "utf8")
const forgotPasswordTemplateSource = fs.readFileSync(path.join(__dirname, "./mail-templates/forgot-password.hbs"), "utf8")
const welcomeTemplateSource = fs.readFileSync(path.join(__dirname, "./mail-templates/welcome.hbs"), "utf8")
const disburseFundsByUserTemplateSource = fs.readFileSync(path.join(__dirname, "./mail-templates/disburseFunds.hbs"), "utf8")
const walletFundedByUserTemplateSource = fs.readFileSync(path.join(__dirname, "./mail-templates/walletFunded.hbs"), "utf8")

// retraction template source
const retractionMilestoneTemplateSource = fs.readFileSync(path.join(__dirname, "./mail-templates/retraction-for-milestone.hbs"), "utf8")
const retractionContractTemplateSource = fs.readFileSync(path.join(__dirname, "./mail-templates/retraction-for-contract.hbs"), "utf8")
const fundedContractTemplateSource = fs.readFileSync(path.join(__dirname, "./mail-templates/funded-contract.hbs"), "utf8")
const fundedContractByTransferTemplateSource = fs.readFileSync(path.join(__dirname, "./mail-templates/funded-contract-banktransfer.hbs"), "utf8")


const contractTemplate = Handlebars.compile(createContractTemplateSource)
const welcomeTemplate = Handlebars.compile(welcomeTemplateSource)
const fundedContractTemplate = Handlebars.compile(fundedContractTemplateSource)
const fundedContractByTransferTemplate = Handlebars.compile(fundedContractByTransferTemplateSource)
const disburseFundsByUserTemplate = Handlebars.compile(disburseFundsByUserTemplateSource)
const walletFundedByUserTemplate = Handlebars.compile(walletFundedByUserTemplateSource)
const forgotPasswordTemplate = Handlebars.compile(forgotPasswordTemplateSource)

// retraction mails
const retractionMilestoneTemplate = Handlebars.compile(retractionMilestoneTemplateSource)
const retractionContractTemplate = Handlebars.compile(retractionContractTemplateSource)

export default class MailService {
    private transporter: nodemailer.Transporter;

    constructor() {
        this.transporter = nodemailer.createTransport(smtpTransport({
            auth: {
                 user: process.env.SENDGRID_USERNAME,
                 pass: process.env.SENDGRID_PASSWORD
            },
            pool: {
                 pool: true,
                 maxConnections: true
            },
            service: "SendGrid"
        }));
    }

    /**
     * Send mail
     * @param param0 
     */
    sendMail ({to, subject, content}: {to: string, subject: string, content: string}) {
        let options = {
            from: `Spincrow <noreply@legobox.io>`,
            to,
            subject,
            html: content
        }

        return new Promise((resolve, reject) => {
            this.transporter.sendMail(options, (err,info) => {
                if(err)
                    reject(err)
                if(info)
                    resolve(info)
            })
        })

    }

    static sendContractMail = async (input: {email: string, subject: string, context: object}) => {
        const mailService = new MailService();
        return await mailService.sendMail({
            to: input.email,
            subject: input.subject,
            content: contractTemplate(input.context)
        })
    }

    static sendWelcomeMail = async (input: {email: string, subject: string, context: object}) => {
        const mailService = new MailService();
        return await mailService.sendMail({
            to: input.email,
            subject: input.subject,
            content: welcomeTemplate(input.context)
        })
    }

    static forgotPasswordMail = async (input: {email: string, subject: string, context: object}) => {
        const mailService = new MailService();
        return await mailService.sendMail({
            to: input.email,
            subject: input.subject,
            content: forgotPasswordTemplate(input.context)
        })
    }

    static sendDisburseMail = async (input: {email: string, subject: string, context: object}) => {
        const mailService = new MailService();
        return await mailService.sendMail({
            to: input.email,
            subject: input.subject,
            content: disburseFundsByUserTemplate(input.context)
        })
    }

    static sendWalletFundedMail = async (input: {email: string, subject: string, context: object}) => {
        const mailService = new MailService();
        return await mailService.sendMail({
            to: input.email,
            subject: input.subject,
            content: walletFundedByUserTemplate(input.context)
        })
    }

    static sendRetractionMailForMilestone = async (input: {email: string, subject: string, context: object}) => {
        const mailService = new MailService();
        return await mailService.sendMail({
            to: input.email,
            subject: input.subject,
            content: retractionMilestoneTemplate(input.context)
        })
    }

    static sendRetractionMailForContract = async (input: {email: string, subject: string, context: object}) => {
        const mailService = new MailService();
        return await mailService.sendMail({
            to: input.email,
            subject: input.subject,
            content: retractionContractTemplate(input.context)
        })
    }

    static fundedContract = async (input: {email: string, subject: string, context: object}) => {
        const mailService = new MailService();
        return await mailService.sendMail({
            to: input.email,
            subject: input.subject,
            content: fundedContractTemplate(input.context)
        })
    }

    static fundedContractByTransfer = async (input: {email: string, subject: string, context: object}) => {
        const mailService = new MailService();
        return await mailService.sendMail({
            to: input.email,
            subject: input.subject,
            content: fundedContractByTransferTemplate(input.context)
        })
    }
    
}