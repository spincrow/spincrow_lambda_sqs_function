'use strict';
import MailService from "./helpers/mail-service";

// console.log('Loading function');

const handler = async (event: any) => {
    console.log('Received event:', JSON.stringify(event, null, 2));
    for (const { messageId, body } of event.Records) {
        console.log('SQS message %s: %j', messageId, body);
        let mailParams = JSON.parse(body)
        switch (mailParams.type) {
            case "send_welcome_mail":
                await MailService.sendWelcomeMail({...mailParams});
                break;
            case "forgot_password_mail":
                await MailService.forgotPasswordMail({...mailParams});
                break;
            case "send_contract_mail":
                await MailService.sendContractMail({...mailParams});
                break;
            case "send_retraction_mail_milestone":
                await MailService.sendRetractionMailForMilestone({...mailParams});
                break;
            case "send_retraction_mail_contracts":
                await MailService.sendRetractionMailForContract({...mailParams});
                break;
            case "fund_contract_mail":
                await MailService.fundedContract({...mailParams});
                break;
            case "funded_contract_by_transfer":
                await MailService.fundedContractByTransfer({...mailParams});
                break;
            case "send_wallet_funded_mail":
                await MailService.sendWalletFundedMail({...mailParams});
                break;
            case "send_disburse_mail":
                await MailService.sendDisburseMail({...mailParams});
                break;
            case "send_wallet_funded_mail":
                await MailService.sendWalletFundedMail({...mailParams});
                break;
        
            default:
                throw new Error("mail type not available")
                break;
        }
    }
    return `Successfully processed`;
}

export default handler