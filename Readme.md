# Spincrow SQS Lambda micro service
> A micro service for managing all spincrow emails.

### Properties.
- Language: Tyscript(javascript)
- Mailer: NodeMailer and Sendgrid
- Queuing service: SQS